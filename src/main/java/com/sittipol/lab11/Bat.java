package com.sittipol.lab11;

public class Bat extends Animal implements Flyable {

    public Bat(String name) {
        super(name, 2);
    }

    public String toString() {
        return "Bat (" + this.getName() + ")";
    }

    @Override
    public void fly() {
        System.out.println(this + " fly.");

    }

    @Override
    public void landing() {
        System.out.println(this + " landing.");
    }

    @Override
    public void takeoff() {
        System.out.println(this + " takeoff.");

    }

    @Override
    public void eat() {
        // TODO Auto-generated method stub

    }

    @Override
    public void sleep() {
        // TODO Auto-generated method stub

    }
}
