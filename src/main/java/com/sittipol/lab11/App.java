package com.sittipol.lab11;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        Bat bat1 = new Bat("Batman");
        bat1.eat();
        bat1.sleep();
        bat1.takeoff();
        bat1.fly();
        bat1.landing();
        System.out.println("================================");

        Fish fish1 = new Fish("Shark");
        fish1.eat();
        fish1.sleep();

        System.out.println("================================");
        Plane plane1 = new Plane("Jet", " Jet engine ");
        plane1.takeoff();
        plane1.fly();
        plane1.landing();

        System.out.println("================================");
        Crocodile crocodile1 = new Crocodile("Meow");
        crocodile1.crawl();
        crocodile1.eat();
        crocodile1.sleep();
        crocodile1.swim();

        System.out.println("================================");
        Snake snake1 = new Snake("Black Mamba");
        snake1.crawl();
        snake1.eat();
        snake1.sleep();

        System.out.println("================================");
        Submarine submarine1 = new Submarine("Going Marry", "Going Marry engine");
        submarine1.swim();

        System.out.println("================================");
        Cat cat1 = new Cat("Tom");
        cat1.eat();
        cat1.sleep();
        cat1.walk();
        cat1.run();

        System.out.println("================================");
        Dog dog1 = new Dog("Quilen");
        dog1.eat();
        dog1.sleep();
        dog1.walk();
        dog1.run();

        System.out.println("================================");
        Bird bird1 = new Bird("Lupin");
        bat1.eat();
        bird1.fly();
        bird1.takeoff();
        bird1.landing();
        bird1.sleep();

        System.out.println("================================");
        Human human1 = new Human("Beer");
        human1.eat();
        human1.sleep();
        human1.walk();
        human1.run();

        System.out.println("================================");
        Rat rat1 = new Rat("ratatouille");
        rat1.eat();
        rat1.sleep();
        rat1.walk();
        rat1.run();

        System.out.println("================================");
        Flyable[] flyableObjects = { bat1, plane1 };
        for (int i = 0; i < flyableObjects.length; i++) {
            flyableObjects[i].takeoff();
            flyableObjects[i].fly();
            flyableObjects[i].landing();
        }
        System.out.println("================================");

        Crawlable[] crawlableObjects = {snake1, crocodile1};
        for(int i=0; i<crawlableObjects.length; i++) {
            crawlableObjects[i].crawl();
        }
        System.out.println("================================");

        Swimable[] swimableObjects = {crocodile1, submarine1};
        for(int i=0; i<swimableObjects.length; i++) {
            swimableObjects[i].swim();
        }
        System.out.println("================================");

        Walkable[] walkableObjects = {human1, dog1, cat1, rat1};
        for(int i=0; i<walkableObjects.length; i++) {
            walkableObjects[i].walk();
            walkableObjects[i].run();
        }
        System.out.println("================================");

    }
}
