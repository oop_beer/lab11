package com.sittipol.lab11;

public class Submarine  extends Vahicle implements Swimable{

    public Submarine(String name, String engineName) {
        super(name, engineName);
        
    }

    @Override
    public void swim() {
        System.out.println(this + " swim. ");
        
    }
    @Override
    public String toString() {
        return "SubMarine (" + this.getName() + ") " + "engine ("+ getEngineName() + ")";
    }
}

